import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  list_countries = [];
  for_filter_list = [];
  phone_number = '';
  calling_code = '';
  error_country_code = false;
  error_phone_number = false;
  valid = 'primary';
  lWidth = 0;
  constructor(private http: HttpClient) {}
  ngOnInit() {
    this.get_countries();

    this.lWidth = window.screen.width;
    console.log(this.lWidth);
  }

  get_countries() {
    this.get('https://restcountries.com/v2/all').subscribe((data) => {
      this.list_countries = data;
      this.for_filter_list = JSON.parse(JSON.stringify(this.list_countries));
    });
  }

  validate() {
    this.error_country_code = false;
    this.error_phone_number = false;
    var err = 0;
    if (!this.calling_code || this.calling_code.length < 1) {
      this.valid = 'warn';
      this.error_country_code = true;
      err += 1;
    }

    if (!this.phone_number || this.phone_number.length < 6) {
      this.valid = 'warn';
      this.error_phone_number = true;
      err += 1;
    }
    if (err > 0) {
      return;
    }
    this.get(
      'https://apilayer.net/api/validate?access_key=eb588dbf70cb81df1c8d374269db9d18&number=' +
        this.calling_code +
        this.phone_number +
        '&format=1'
    ).subscribe((data) => {
      if (data['valid'] == false) {
        this.error_phone_number = true;
        this.valid = 'warn';
      } else {
        this.valid = 'success';
      }
    });
  }

  filter_country(event: any) {
    var key = event.target.value.toLowerCase();

    this.for_filter_list = JSON.parse(JSON.stringify(this.list_countries));
    this.for_filter_list = this.for_filter_list.filter(
      (r1: any) =>
        r1['alpha2Code'].toLowerCase().includes(key) ||
        r1['alpha3Code'].toLowerCase().includes(key) ||
        r1['demonym'].toLowerCase().includes(key) ||
        r1['name'].toLowerCase().includes(key) ||
        r1['numericCode'].toLowerCase().includes(key)
    );
  }

  get(tab: string): Observable<any> {
    var result = this.http.get(tab);

    return result;
  }
}
